# nslice

Structures for interpreting slices of variable length as arrays.

`nslice` provides `MinSlice` and `ExactSlice` for representing slices known to have
either exactly or at least some compile-time-known number of values.

This is useful when dealing with a slice whose length you expect to be exactly
or at least some particular length;
rather than making runtime checks on each access,
you can check the length once and access values freely with no copying.

```rust
# use nslice::MinSlice;

let slice = &[1, 2, 3, 4, 5, 6];
let minslice: &MinSlice<_, 3> = MinSlice::from_slice(slice).unwrap();
assert_eq!(minslice.tail.len(), 3);
assert_eq!(minslice.head[0], 1);
assert_eq!(minslice.tail[2], 6);
```

## MSRV

This project supports Rust 1.56.1 and onward.

## License

This project is licensed MIT.

